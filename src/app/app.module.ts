import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CalendarModule} from 'primeng/calendar';
import {FormsModule} from '@angular/forms';
import {MenuModule} from 'primeng/menu';
import {TabMenuModule} from 'primeng/tabmenu';
import {AppRoutesModule} from './app-routes.module';
import {ButtonModule} from 'primeng/button';
import {AccordionModule} from 'primeng/accordion';
import {StockItemGeneratorComponent} from './Stock/stock-item-generator/stock-item-generator.component';
import {StockModule} from './Stock/stock.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CalendarModule,
    FormsModule,
    MenuModule,
    TabMenuModule,
    AppRoutesModule,
    ButtonModule,
    AccordionModule,
    StockModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
