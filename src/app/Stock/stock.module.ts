import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockItemGeneratorComponent } from '../Stock/stock-item-generator/stock-item-generator.component';

@NgModule({
  declarations: [StockItemGeneratorComponent],
  imports: [
    CommonModule
  ]
})
export class StockModule { }
