import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockItemGeneratorComponent } from './stock-item-generator.component';

describe('StockItemGeneratorComponent', () => {
  let component: StockItemGeneratorComponent;
  let fixture: ComponentFixture<StockItemGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockItemGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockItemGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
