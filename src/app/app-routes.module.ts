import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockItemGeneratorComponent} from './Stock/stock-item-generator/stock-item-generator.component';

const appRoutes: Routes = [
  // {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: 'stock/create', component: StockItemGeneratorComponent},
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutesModule {
}
