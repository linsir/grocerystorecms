import {Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  navItems: MenuItem[];

  activeItem: MenuItem;

  ngOnInit() {
    this.navItems = [
      {label: '添加货品', icon: 'pi pi-pencil', routerLink: ['stock/create']},
      {label: '查询货品', icon: 'pi pi-search', routerLink: ['/']},
      {label: '使用说明', icon: 'fa fa-fw fa-book', routerLink: ['/']},
      {label: '技术支持', icon: 'fa fa-fw fa-support', routerLink: ['/']},
    ];

    this.activeItem = this.navItems[0];
  }

  // ngDoCheck() {
  //   console.log(this.activeItem.label);
  // }

  closeItem(event, index) {
    this.navItems = this.navItems.filter((item, i) => i !== index);
    event.preventDefault();
  }

  redirectToUrl($event: MouseEvent) {
    console.log(event);
  }
}
